import gulp from 'gulp';
import cwebp from 'gulp-cwebp';
import { distDir } from '../gulpfile.babel';

gulp.task('webp', (done) => {
  gulp.src('./src/resources/images/**')
    .pipe(cwebp())
    .pipe(gulp.dest(`${distDir}/images/`));
  done();
});
