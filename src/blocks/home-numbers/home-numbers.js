import isInViewport from '../../scripts/modules/isInViewport';

const animateNumbers = (elem, from = 1, to = 1) => {
  const count = elem;
  let now;
  let then = Date.now();
  let elapsed;
  const step = (to - from) / 12;
  let current = from;
  const draw = (cur) => {
    count.innerHTML = `${Math.floor(cur)}`;
  };

  const condition = () => (from < to && current < to) || (from > to && current > to);
  const conditionStop = () => (from < to && current >= to) || (from > to && current <= to);

  const tick = () => {
    const raf = requestAnimationFrame(tick);
    now = Date.now();
    elapsed = now - then;
    if (condition() && elapsed > 1000 / 12) {
      then = now - (elapsed % (1000 / 12));
      draw(current);
      current += step;
    }
    if (conditionStop()) {
      draw(to);
      cancelAnimationFrame(raf);
    }
  };

  draw(current);
  tick();
};

const homeNumbers = () => {
  const numbers = document.querySelector('.home-numbers');
  if (!numbers) return;
  const numbersItems = Array.from(numbers.querySelectorAll('.home-numbers__item-counter'))
    .filter((item) => item.dataset.counter);
  numbersItems.forEach((item) => {
    const elem = item;
    elem.innerHTML = elem.dataset.counter;
    const widthTo = parseFloat(getComputedStyle(item).width);
    elem.innerHTML = elem.dataset.counterFrom || 0;
    const widthFrom = parseFloat(getComputedStyle(item).width);
    elem.style.width = widthTo > widthFrom ? `${widthTo}px` : '';
    const animate = () => {
      requestAnimationFrame(() => {
        const { bottom } = item.getBoundingClientRect();
        if (isInViewport(item) && bottom <= (window.innerHeight - (window.innerHeight / 6))) {
          animateNumbers(item, +item.innerHTML, +item.dataset.counter);
          document.removeEventListener('scroll', animate);
        }
      });
    };
    document.addEventListener('scroll', animate);
  });
};

export default homeNumbers;
