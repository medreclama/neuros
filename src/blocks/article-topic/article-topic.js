const articleTopic = document.querySelector('.article-topic');

const articleContent = () => {
  if (!articleTopic) return;
  const contentLinkShow = document.createElement('a');
  contentLinkShow.href = '#';
  contentLinkShow.innerHTML = 'Показать все';
  articleTopic.append(contentLinkShow);

  const contentItems = Array.from(articleTopic.querySelectorAll('.article-topic__item'));
  const LINKS_NUM = 10;
  const contentHiddenItems = contentItems.slice(LINKS_NUM, contentItems.length);
  const showLink = () => {
    contentHiddenItems.forEach((contentHiddenItem) => {
      const item = contentHiddenItem;
      item.style.display = 'none';
      item.classList.add('hidden');
      contentLinkShow.addEventListener('click', (evt) => {
        evt.preventDefault();
        if (item.classList.contains('hidden')) {
          contentLinkShow.innerHTML = 'Скрыть';
          item.style.display = 'list-item';
        } else {
          contentLinkShow.innerHTML = 'Показать все';
          item.style.display = 'none';
        }
        item.classList.toggle('hidden');
      });
    });
  };

  if (contentItems.length > (LINKS_NUM)) {
    showLink();
  } else if (contentLinkShow) {
    contentLinkShow.style.display = 'none';
  }
};

export default articleContent;
