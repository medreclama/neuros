const treatment = document.querySelector('.home-treatment');

const homeTreatment = () => {
  if (!treatment) return;
  const treatmentLayer = treatment.querySelector('.home-treatment__layer');
  const treatmentBlocks = treatment.querySelectorAll('.home-treatment__inner');
  const treatmentBlock = treatment.querySelector('.home-treatment__block');
  const treatmentImg = treatment.querySelector('.home-treatment__layer-image');

  treatmentLayer.addEventListener('mouseover', (evt) => {
    if (evt.target.id) treatmentImg.setAttribute('href', `/images/${evt.target.id}.webp`);
  });

  treatmentLayer.addEventListener('mouseout', () => {
    if (treatmentLayer.dataset.selected === '') treatmentImg.setAttribute('href', '/images/treatment.webp');
    else treatmentImg.setAttribute('href', `/images/${treatmentLayer.dataset.selected}.webp`);
  });

  treatmentBlock.addEventListener('mouseover', (evt) => {
    if (evt.target.tagName === 'BUTTON') {
      treatmentImg.setAttribute('href', `/images/${evt.target.closest('div').dataset.img}.webp`);
    }
  });

  treatmentBlock.addEventListener('mouseout', (evt) => {
    if (evt.target.tagName === 'BUTTON') {
      if (treatmentLayer.dataset.selected === '') treatmentImg.setAttribute('href', '/images/treatment.webp');
      else treatmentImg.setAttribute('href', `/images/${treatmentLayer.dataset.selected}.webp`);
    }
  });

  treatmentLayer.addEventListener('click', (evt) => {
    if (evt.target.id) {
      treatmentBlocks.forEach((block) => {
        block.classList.remove('home-treatment__inner--active');
        if (evt.target.id === block.dataset.img) {
          block.classList.add('home-treatment__inner--active');
          treatmentLayer.dataset.selected = evt.target.id;
        }
      });
    }
  });

  treatmentBlock.addEventListener('click', (evt) => {
    if (evt.target.tagName === 'BUTTON') {
      const dataImg = evt.target.closest('div').dataset.img;
      treatmentLayer.dataset.selected = dataImg;
      treatmentImg.setAttribute('href', `/images/${treatmentLayer.dataset.selected}.webp`);
      evt.target.closest('div').classList.add('home-treatment__inner--active');
      treatmentBlocks.forEach((block) => {
        if (dataImg !== block.dataset.img) block.classList.remove('home-treatment__inner--active');
      });
    }
  });
};

export default homeTreatment;
