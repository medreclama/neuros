const headerSwitcher = () => {
  const switcher = document.querySelector('.header-switcher');
  switcher.addEventListener('click', (e) => {
    ['header-switcher--active', 'header__switcher--active'].forEach((c) => e.target.classList.toggle(c));
  });
};

export default headerSwitcher;
