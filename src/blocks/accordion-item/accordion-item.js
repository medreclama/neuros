const accordionItem = () => {
  const items = document.querySelectorAll('.accordion-item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      if (event.target.closest('.accordion-item__header')) item.classList.toggle('accordion-item--active');
    });
  });
};

export default accordionItem;
