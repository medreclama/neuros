const reviewItem = () => {
  const review = document.querySelectorAll('.review-item__body');
  Array.from(review).forEach((item) => {
    if (item.innerText.length > 500) {
      const itemContent = item.innerHTML;
      let showMoreButton;
      let hideButton;
      const hide = (event) => {
        if (event) event.preventDefault();
        const beginLastSpace = item.innerText.slice(0, 420).lastIndexOf(' ') - 1;
        const begin = item.innerText.slice(0, beginLastSpace + 1);
        Array.from(item.children).forEach((child) => child.remove());
        item.insertAdjacentHTML('beforeend', `<p>${begin}... <a href="#" class="review-item__show-more">Читать&nbsp;полностью</a></p>`);
        showMoreButton = item.querySelector('.review-item__show-more');
        // eslint-disable-next-line no-use-before-define
        showMoreButton.addEventListener('click', show);
      };
      const show = (event) => {
        event.preventDefault();
        showMoreButton.remove();
        item.firstElementChild.remove();
        item.insertAdjacentHTML('beforeend', itemContent);
        item.lastElementChild.insertAdjacentHTML('beforeend', ' <a href="#" class="review-item__hide">Свернуть</a>');
        hideButton = item.querySelector('.review-item__hide');
        hideButton.addEventListener('click', hide);
      };
      hide();
    }
  });
};

export default reviewItem;
