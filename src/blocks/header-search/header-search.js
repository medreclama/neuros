const headerSearch = () => {
  const search = document.querySelector('.header-search');
  search.addEventListener('click', (e) => {
    if (e.target.classList.contains('header-search__label')) {
      if (!search.classList.contains('header-search--active')) {
        search.classList.add('header-search--active');
        setTimeout(() => search.classList.add('header-search--appearing'), 1);
      } else {
        search.classList.remove('header-search--appearing');
        setTimeout(() => search.classList.remove('header-search--active'), 400);
      }
    }
  });
};

export default headerSearch;
