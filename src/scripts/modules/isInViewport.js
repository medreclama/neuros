const isInViewport = (element) => {
  const rect = element.getBoundingClientRect();
  return (
    rect.bottom >= 0
    && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
  );
};

export default isInViewport;
