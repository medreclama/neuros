import Validator from './Validator';
import { showModal } from '../../../blocks/modal/modal';

export default class Form {
  constructor(form) {
    this.$form = form;
    this.$submit = this.$form.querySelector('[type="submit"]');
    this.$form.addEventListener('submit', (event) => this.onSubmit(event));
    this.$modal = document.getElementById('ajax-response');
    this.$modalTitle = this.$modal.querySelector('[data-title]');
    this.$modalBody = this.$modal.querySelector('[data-body]');
    this.init();
  }

  init() {
    this.isValidate = this.$form.dataset.isValidate === 'true';
    this.isAnswerInPopup = this.$form.dataset.isAnswerInPopup === 'true';

    this.answer = {
      success: { title: 'Благодарим за обращение.', text: 'С Вами свяжутся в ближайшее время!' },
      error: { title: 'Ошибка', text: 'Пожалуйста повторите отправку формы &hellip;' },
    };

    if (this.$form.dataset.answerSuccess) {
      const [title, text] = Form.splitAnswer(this.$form.dataset.answerSuccess);
      this.answer.success.title = title;
      this.answer.success.text = text;
    }

    if (this.$form.dataset.answerError) {
      const [title, text] = Form.splitAnswer(this.$form.dataset.answerError);
      this.answer.error.title = title;
      this.answer.error.text = text;
    }
  }

  static splitAnswer(string) {
    return string.split('|').map((str) => str.trim());
  }

  onSubmit(event) {
    event.preventDefault();

    if (this.isValidate) {
      const validator = new Validator(this.$form);
      if (!validator.validateFields()) {
        return false;
      }
    }

    this.processRequest()
      .then((data) => (data.result === 'success' ? this.onSuccessResponse(data) : this.onErrorResponse(data)))
      .catch((error) => this.onErrorResponse(error.message))
      .finally(() => this.showResponse());

    return true;
  }

  async processRequest() {
    this.spinnerAdd();

    const response = await fetch('/ajax/index.php', {
      method: 'POST',
      body: new FormData(this.$form),
    });

    return response.json();
  }

  onSuccessResponse() {
    if (this.isAnswerInPopup) {
      this.$modalTitle.innerHTML = this.answer.success.title;
      this.$modalTitle.classList.add('modal-title-success');
      this.$modalBody.innerHTML = this.answer.success.text;
    } else {
      this.message = Form.getInlineMessage(this.answer.success.title, this.answer.success.text, 'success');
    }

    if (this.$form.dataset.onSuccess) {
      /* eslint no-eval: "off" */
      eval(this.$form.dataset.onSuccess);
    }
  }

  onErrorResponse(error) {
    let errorText = this.answer.error.text;
    const i = document.createElement('i');
    i.textContent = typeof error === 'string' ? error : error.message;
    errorText = `<i>${i.innerHTML}</i> <br><br> ${errorText}`;

    if (this.isAnswerInPopup) {
      this.$modalTitle.innerHTML = this.answer.error.title;
      this.$modalTitle.classList.add('modal-title-danger');
      this.$modalBody.innerHTML = errorText;
    } else {
      this.message = Form.getInlineMessage(this.answer.error.title, errorText, 'danger');
    }
  }

  showResponse() {
    this.spinnerRemove();

    if (this.isAnswerInPopup) {
      showModal(this.$modal);
      this.$form.reset();
    } else {
      this.$form.parentElement.append(this.message);
      this.$form.remove();
    }
  }

  static getInlineMessage(title, text, type) {
    const answer = document.createElement('div');
    answer.className = `answer answer-${type} text-align-center`;
    answer.insertAdjacentHTML('beforeend', `<h3>${title}</h3>`);
    answer.insertAdjacentHTML('beforeend', `<p>${text}</p>`);
    return answer;
  }

  spinnerAdd() {
    this.$submit.disabled = true;
    this.$submit.classList.add('disabled');
    this.$modalTitle.classList.remove('modal-title-success', 'modal-title-danger');
  }

  spinnerRemove() {
    this.$submit.disabled = false;
    this.$submit.classList.add('disabled');
  }
}
