import IMask from 'imask';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import headerSearch from '../blocks/header-search/header-search';
import headerSwitcher from '../blocks/header-switcher/header-switcher';
import Slider from '../blocks/slider/slider';
import modal from '../blocks/modal/modal';
import Form from './modules/form/Form';
import reviewItem from '../blocks/review-item/review-item';
import consultationCategories from '../blocks/consultation-categories/consultation-categories';
import counters from './modules/counters';
import homeNumbers from '../blocks/home-numbers/home-numbers';
import accordionItem from '../blocks/accordion-item/accordion-item';
import articleContent from '../blocks/article-topic/article-topic';
import homeTreatment from '../blocks/home-treatment/home-treatment';

headerNavigation();
headerSearch();
headerSwitcher();
reviewItem();
consultationCategories();
homeNumbers();
accordionItem();
articleContent();
modal();
homeTreatment();

const inputTel = document.querySelectorAll('input[type="tel"]');
inputTel.forEach((input) => {
  IMask(input, { mask: '+7 (000) 000-00-00' });
});

const threeColsSliderSettings = {
  breakpoints: {
    800: {
      spaceBetween: 40,
      slidesPerView: 3,
      slidesPerGroup: 1,
    },
    480: {
      spaceBetween: 40,
      slidesPerView: 2,
      slidesPerGroup: 1,
    },
  },
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
};

const doctorsSliderSettings = {
  breakpoints: {
    1200: {
      spaceBetween: 40,
      slidesPerView: 3,
      slidesPerGroup: 1,
    },
    600: {
      spaceBetween: 40,
      slidesPerView: 2,
      slidesPerGroup: 1,
    },
  },
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
};

const twoColsSliderSettings = {
  breakpoints: {
    800: {
      spaceBetween: 40,
      slidesPerView: 2,
      slidesPerGroup: 1,
    },
  },
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
};

const certificatesSliderSettings = {
  breakpoints: {
    1000: {
      spaceBetween: 10,
      slidesPerView: 6,
      slidesPerGroup: 1,
    },
    800: {
      spaceBetween: 10,
      slidesPerView: 4,
      slidesPerGroup: 1,
    },
  },
  spaceBetween: 0,
  slidesPerView: 2,
  slidesPerGroup: 1,
};

const oneColSliderSettings = {
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
};

const slider = document.querySelector('.slider .slider__slides');
if (slider) {
  const sliderInstance = new Slider(slider, oneColSliderSettings);
  sliderInstance.init();
}

const interiorsSlider = document.querySelector('.slider-interior .slider__slides');
if (interiorsSlider) {
  const interiorsSliderInstance = new Slider(interiorsSlider, threeColsSliderSettings);
  interiorsSliderInstance.init();
}

const doctorsSlider = document.querySelector('.doctors-slider .slider__slides');
if (doctorsSlider) {
  const doctorsSliderInstance = new Slider(doctorsSlider, doctorsSliderSettings);
  doctorsSliderInstance.init();
}

const reviewsSlider = document.querySelector('.slider-reviews .slider__slides');
if (reviewsSlider) {
  const reviewsSliderInstance = new Slider(reviewsSlider, twoColsSliderSettings);
  reviewsSliderInstance.init();
}

const methodsSlider = document.querySelector('.slider-methods .slider__slides');
if (methodsSlider) {
  const methodsSliderInstance = new Slider(methodsSlider);
  methodsSliderInstance.init();
}

const advantagesSlider = document.querySelector('.slider-advantages .slider__slides');
if (advantagesSlider) {
  const advantagesSliderInstance = new Slider(advantagesSlider, threeColsSliderSettings);
  advantagesSliderInstance.init();
}

const certificatesSlider = document.querySelector('.slider-certificates .slider__slides');
if (certificatesSlider) {
  const certificatesSliderInstance = new Slider(certificatesSlider, certificatesSliderSettings);
  certificatesSliderInstance.init();
}

const sliderNews = document.querySelector('.slider-news .slider__slides');
if (sliderNews) {
  const sliderNewsInstance = new Slider(sliderNews, threeColsSliderSettings);
  sliderNewsInstance.init();
}

const directionsSlider = document.querySelector('.slider-directions .slider__slides');
if (directionsSlider) {
  const directionsSliderInstance = new Slider(directionsSlider, {
    spaceBetween: 40,
    breakpoints: {
      480: {
        slidesPerView: 2,
      },
      1100: {
        slidesPerView: 3,
      },
    },
  });
  directionsSliderInstance.init();
}

document.querySelectorAll('form.form-ajax').forEach((form) => new Form(form));

const countersCodeHead = `
<script type="text/javascript" data-skip-moving="true">
    var __cs = __cs || [];
    __cs.push(["setCsAccount", "xAyLscsKf_SdKkteNwbNIglqG9RxDFMH"]);
</script>
<script type="text/javascript" data-skip-moving="true">
    var __cs = __cs || [];
    __cs.push(["setCsAccount", "vQxcOWB26gxBg6ncPcDenLgH2Okg6oqT"]);
</script>
<script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js" data-skip-moving="true"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2T2NC1N4DL" data-skip-moving="true"></script>
<script data-skip-moving="true">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'G-2T2NC1N4DL');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(51762461, "init", {
        id:51762461,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
    // ym(51762461,'reachGoal','gratitude');
    // ym(51762461,'reachGoal','amozapis');
    // ym(51762461,'reachGoal','amofeedback');
    // ym(51762461,'reachGoal','callback');
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51762461" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
`;
const countersCodeBody = `
<script data-skip-moving="true">(function(t, p) {window.Marquiz ? Marquiz.add([t, p]) : document.addEventListener('marquizLoaded', function() {Marquiz.add([t, p])})})('Pop', {id: '62a0236139cee5004f8f0d8d', title: 'Пройти тест', text: 'Подойдет ли вам лечение в нашем центре?', delay: 50, textColor: '#fff', bgColor: '#4fb14a', svgColor: '#fff', closeColor: '#ffffff', bonusCount: 2, bonusText: 'Вам доступны бонусы', type: 'full', position: 'position_top'})</script>
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);
